var CSSEmbedder, fs, mime, pathLib;

fs = require('fs');

pathLib = require('path');

mime = require('mime');

function CSSEmbedder(cfg) {
    config = (cfg.plugins && cfg.plugins.cssembed) || {};
    var _config = {};

    // default options:

    // max file size 32k, for IE8 compatibility
    _config.maxSize = parseInt(config.maxSize);
    if (isNaN(_config.maxSize)) {
        _config.maxSize = 32 * 1024;
    }

    this.config = _config;
    null;
}

CSSEmbedder.prototype.brunchPlugin = true;

CSSEmbedder.prototype.type = 'stylesheet';

CSSEmbedder.prototype.extension = 'css';

CSSEmbedder.prototype.statementStack = [];

CSSEmbedder.prototype.compile = function(data, path, callback) {
    var config = this.config;
    var newData, r, rootDir;
    rootDir = pathLib.dirname(path);
    r = new RegExp('url\\((.*?)\\)', 'g');
    newData = data.replace(r, function(match, p1) {
        var base64Data, e, fileName, mimeType, stats;
        // trimming quote symbols, if any
        if (p1[0] === '\'') {
            if (p1[p1.length - 1] === '\'') {
                p1 = p1.substring(1, p1.length - 1);
            } else {
                return match;
            }
        } else if (p1[0] === '"') {
            if (p1[p1.length - 1] === '"') {
                p1 = p1.substring(1, p1.length - 1);
            } else {
                return match;
            }
        }
        // absolute file path, skipping
        if (p1[0] === '/') {
            return match;
        }
        // external resource, skipping
        if (p1.match("(https?|ftp):\/\/")) {
            return match;
        }
        fileName = pathLib.join(rootDir, p1);
        try {
            stats = fs.statSync(fileName);
            if ((config.maxSize > 0) && (stats.size > config.maxSize)) {
                return match;
            }
            // determining MIME type
            mimeType = mime.lookup(fileName);
            // encoding
            base64Data = fs.readFileSync(fileName).toString('base64');
            return 'url(\'data:' + mimeType + ';base64,' + base64Data + '\')';
        } catch (_error) {
            e = _error;
            // couldn't read data, skipping
            return match;
        }
        return match;
    });
    return callback(null, newData);
};

module.exports = CSSEmbedder;
