# CSSEmbed-brunch

CSS Preprocessor for [brunch](http://brunch.io). Embeds images and other assets into css stylesheets as [data URIs](https://en.wikipedia.org/wiki/Data_URI_scheme).

## Options:

* `maxSize`: maximum size of embedded file, default is 32k for IE8 compatibility. Set this to 0 to disable the check completely.

```coffeescript
config =
  plugins:
    cssembed:
      maxSize: 0
```

